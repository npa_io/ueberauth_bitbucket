defmodule UeberauthBitbucket.Mixfile do
  use Mix.Project

  @version "1.0.0"

  def project do
    [app: :ueberauth_bitbucket,
     version: @version,
     name: "Ueberauth Bitbucket",
     package: package(),
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     source_url: "https://bitbucket.com/npa_io/ueberauth_bitbucket",
     homepage_url: "https://bitbucket.com/npa_io/ueberauth_bitbucket",
     description: description(),
     deps: deps(),
     docs: docs()]
  end

  def application do
    [applications: [:logger, :ueberauth, :oauth2]]
  end

  defp deps do
    [{:ueberauth, "~> 0.4"},
     {:oauth2, "~> 0.8"},

     # docs dependencies
     {:earmark, "~> 0.2", only: :dev},
     {:ex_doc, ">= 0.0.0", only: :dev}]
  end

  defp docs do
    [extras: ["README.md"]]
  end

  defp description do
    "An Ueberauth strategy for using Bitbucket to authenticate your users."
  end

  defp package do
    [files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["Jakob Dam Jensen"],
      licenses: ["MIT"],
      links: %{"Bitbucket": "https://bitbucket.com/npa_io/ueberauth_bitbucket"}]
  end
end
