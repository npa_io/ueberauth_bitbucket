# Überauth Bitbucket

> Bitbucket OAuth2 strategy for Überauth.

## Installation

1. Setup your application at [Bitbucket Developer](https://developer.bitbucket.com).

1. Add `:ueberauth_bitbucket` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ueberauth_bitbucket, "~> 0.4"}]
    end
    ```

1. Add the strategy to your applications:

    ```elixir
    def application do
      [applications: [:ueberauth_bitbucket]]
    end
    ```

1. Add Bitbucket to your Überauth configuration:

    ```elixir
    config :ueberauth, Ueberauth,
      providers: [
        bitbucket: {Ueberauth.Strategy.Bitbucket, []}
      ]
    ```

1.  Update your provider configuration:

    ```elixir
    config :ueberauth, Ueberauth.Strategy.Bitbucket.OAuth,
      client_id: System.get_env("BITBUCKET_CLIENT_ID"),
      client_secret: System.get_env("BITBUCKET_CLIENT_SECRET")
    ```

1.  Include the Überauth plug in your controller:

    ```elixir
    defmodule MyApp.AuthController do
      use MyApp.Web, :controller

      pipeline :browser do
        plug Ueberauth
        ...
       end
    end
    ```

1.  Create the request and callback routes if you haven't already:

    ```elixir
    scope "/auth", MyApp do
      pipe_through :browser

      get "/:provider", AuthController, :request
      get "/:provider/callback", AuthController, :callback
    end
    ```

1. You controller needs to implement callbacks to deal with `Ueberauth.Auth` and `Ueberauth.Failure` responses.

For an example implementation see the [Überauth Example](https://bitbucket.com/ueberauth/ueberauth_example) application.

## Calling

Depending on the configured url you can initial the request through:

    /auth/bitbucket

Or with options:

    /auth/bitbucket?scope=user,public_repo

By default the requested scope is "user,public\_repo". Scope can be configured either explicitly as a `scope` query value on the request path or in your configuration:

```elixir
config :ueberauth, Ueberauth,
  providers: [
    bitbucket: {Ueberauth.Strategy.Bitbucket, [default_scope: "user,public_repo,notifications"]}
  ]
```

## License

Please see [LICENSE](https://bitbucket.com/ueberauth/ueberauth_bitbucket/blob/master/LICENSE) for licensing details.
